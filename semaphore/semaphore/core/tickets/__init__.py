from flask import Blueprint, flash, g, redirect
from flask import request, session, url_for, current_app as app


module = Blueprint('tickets', __name__, template_folder="../../templates")

from .views import *
from . import models
