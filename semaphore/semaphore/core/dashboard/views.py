from flask import render_template, url_for, request, g

from semaphore.core.auth.decorators import login_required

from . import module
from semaphore.core.tickets.models import Ticket

@module.route('/', methods=('GET',))
@login_required
def index():
	# Create a list of tiles...
	tickets = Ticket.get_tickets_of_org(g.organization._id).limit(5)
	my_tickets = Ticket.get_tickets_involving(g.organization._id, g.user).limit(5)
	return render_template('dashboard/dashboard_home.html', tickets=tickets , my_tickets=my_tickets)

