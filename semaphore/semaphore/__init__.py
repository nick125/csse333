#!/usr/bin/env python
#
# Bootstraps the instance
#
from importlib import import_module
from flask import Flask
from flask.ext.mongoengine import MongoEngine

import config

def register_module(app, module_name):
	"""
	Registers a module and any global event hooks it might have.
	"""
	module = import_module(module_name)

	if hasattr(module, 'module'):
		app.register_blueprint(module.module)

	if hasattr(module, "before_request_hooks"):
		app.logger.debug('Registering %d before_request hooks for %s' %
				(len(module.before_request_hooks), module))
		for hook in module.before_request_hooks:
			app.before_request(hook)

	if hasattr(module, "after_request_hooks"):
		app.logger.debug('Registering %d after_request hooks for %s' %
				(len(module.after_request_hooks), module))
		for hook in module.after_request_hooks:
			app.after_request(hook)

	if hasattr(module, "FILTERS"):
		app.jinja_env.filters.update(module.FILTERS)

def create_app():
	app = Flask(__name__, static_folder=config.STATIC_PATH)

	app.config.from_object(config)
	app.secret_key = app.config['SECRET_KEY']
	
	app.mongo = MongoEngine(app)

	for mod in config.ENABLED_MODULES:
		register_module(app, mod)

	return app
