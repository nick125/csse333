from flask import current_app as app, g, render_template, redirect, url_for, \
		request

from semaphore.core.auth.decorators import login_required
from semaphore.core.settings.decorators import permission_required

from semaphore.core.settings import permissions

from semaphore.core.auth.models import User
from semaphore.core.tickets.models import Product

from . import module
from .forms import *

__all__ = ["index", "user_browse", "user_add","product_create","product_edit","user_edit","products_browse","organization_save"]

@module.route("/manage")
@permission_required(permissions.ORGANIZATION, permissions.MANAGE)
@login_required
def index():
	return render_template('management/index.html')


@module.route("/manage/users")
@login_required
@permission_required(permissions.USERS, permissions.READ)
def user_browse():
	form = UserForm(request.form)
	users = User.get_all_users(g.organization._id)
	return render_template('management/users/browse_users.html', users=users, form=form)
	
	
@module.route("/manage/users/create")
@login_required
@permission_required(permissions.USERS, permissions.CREATE)
def user_create():
	form = UserForm2(request.form)
	return render_template('management/users/create_users.html', form=form)	

@module.route("/user/profile/edit")
@login_required
def edit_profile():
	user = User.get_user_id(g.user._id)
	if user is None:
		redirect(url_for('dashboard.index'))
	form = UserForm(request.form)
	form.populate_obj(user)
	return render_template('management/users/profile_edit.html', form=form)
	
@module.route("/user/profile/view")
@login_required
def view_profile():
	user = User.get_user_id(g.user._id)
	if user is None:
		redirect(url_for('dashboard.index'))

	return render_template('management/users/view_profile.html', user=user)
	
	
		

@module.route("/manage/users/save",  methods=('GET','POST'))
@login_required
@permission_required(permissions.USERS, permissions.CREATE)
def user_save(methods=('GET','POST')):
	form = UserForm2(request.form)
	if form.validate():
		if len(form.uid.data) > 0:
			# Existing user
			user = User.get_user_id(form.uid.data)
		else:
			# New user
			user = User()
			user.organization = g.organization
			user.active = form.active.data
			user.username = form.name.data
			user.name.first, user.name.last = form.name.data.split(' ')
			user.password = Password().set_password(form.password.data)
		user.save()
		return redirect(url_for('management.user_browse'))
	else:
		print "WTF?!"
		print form.errors
		return render_template('management/users/edit_users.html',
				form=form)
	
	
	
	return render_template('management/products/create_users.html', form=form)	
	
	
@module.route("/manage/users/add", methods=('POST',))
@login_required
@permission_required(permissions.USERS, permissions.CREATE)
def user_add():
	# Find the username given
	user = User.get_user(request.form['name'])
	if user is None:
		flash(_("<strong>The user you tried to create does not exist.</strong>\
		Please check the username and try again."))
		return redirect(url_for('manage.user_browse'))

	user.organizations.append(g.organization)
	user.save()

	return redirect(url_for('management.user_browse'))

@module.route('/manage/users/edit/<id>', methods=('GET','POST'))
@login_required
@permission_required(permissions.USERS, permissions.MODIFY)
def user_edit(id, methods=('GET','POST')):
	user = User.get_user_id(id)
	print user
	form = EditUserForm(request.form, user)
	form.uid.data = id; 
	form.populate_obj(user)
	if request.method == 'POST' and form.validate_on_submit():
		user.active = form.active.data
		name = form.name.data
		name_split = name.split(" ")
		user.name = create_name(name_split[0], name_split[1])
		if form.password is not None:
			user.password = set_password(form.password.data)
		user.save()
		
	return render_template('management/users/edit_users.html', form=form)
	

@module.route('/manage/products')
@login_required
@permission_required(permissions.PRODUCTS, permissions.READ)
def products_browse():
	products = Product.get_all_products(g.organization._id)
	return render_template('management/products/browse_products.html', products=products)
	

@module.route('/manage/products/create')
@login_required
@permission_required(permissions.PRODUCTS, permissions.CREATE)
def product_create():
	form = ProductForm(request.form)
	return render_template('management/products/create_product.html', form=form)

@module.route('/manage/products/edit/<id>')
@login_required
@permission_required(permissions.PRODUCTS, permissions.MODIFY)
def product_edit(id):
	form = ProductForm(request.form, Product.get_product(id))
	form.pid.data = id
	return render_template('management/products/edit_product.html', form=form)

@module.route('/manage/products/save', methods=('POST',))
@login_required
@permission_required(permissions.PRODUCTS, permissions.MODIFY)
def product_save():
	form = ProductForm(request.form)
	if form.validate():
		if len(form.pid.data) > 0:
			# Existing product
			product = Product.get_product(form.pid.data)
		else:
			# New product
			product = Product()
			product.organization = g.organization
			product.active = True
	
		form.populate_obj(product)
		product.save()
		return redirect(url_for('management.products_browse'))
	else:
		print "WTF?!"
		print form.errors
		return render_template('management/products/edit_product.html',
				form=form)

@module.route('/manage/organization/save', methods=('POST',))
@login_required
@permission_required(permissions.ORGANIZATION, permissions.MODIFY)
def organization_save():
	form = OrganizationForm(request.form)
	if form.validate():
		form.populate_obj(g.organization)
		g.organization.save()
	else:
		print form.errors
		return render_template('management/organizations/edit_organization.html',
				form=form);
	return redirect(url_for('dashboard.index'))

@module.route('/manage/organization/edit', methods=('GET','POST'))
@login_required
@permission_required(permissions.ORGANIZATION, permissions.MODIFY)
def organization_edit():
	form = OrganizationForm(request.form, g.organization)
	return render_template('management/organizations/edit_organization.html', form=form)
