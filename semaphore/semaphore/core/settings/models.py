import datetime
from flask import current_app as app
from flaskext.mongoengine import Document
from mongoengine import EmbeddedDocument
from mongoengine import DictField
import config


class Settings(DictField):
	"""
	A settings store for a particular object.
	"""
	pass

class Permissions(DictField):
	"""
	A permissions store for a particular object.
	"""
	pass

