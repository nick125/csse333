from flask import Module, render_template, url_for, request, session, redirect
from flask import flash, current_app as app, g

from pymongo.objectid import ObjectId

from semaphore.core.i18n import ugettext as _
from semaphore.core.auth.decorators import login_required
from semaphore.core.settings.decorators import permission_required
from semaphore.core.organizations.models import Organization

import semaphore.core.settings.permissions


from . import module
from .forms import LoginForm, CreateUserForm, EditUserForm
from .models import User, Password

@module.route('/login/change_organization/<id>')
@login_required
def change_organization(id=None):
	if id is not None:
		# Check if user belongs to organization
		org = Organization.get_organization(id)
		if org is not None and org in g.user.organizations:
			session['organization'] = org._id
			app.logger.debug("Changed '%s' organization to '%s'" %
					(g.user.username, session['organization']))
		else:
			app.logger.debug("User tried to change to non-existent organization?")
	if 'referer' in request.headers: 
		return redirect(request.headers['referer'])
	return redirect(url_for('dashboard.index'))


@module.route('/login', methods=('GET', 'POST'))
def login():
	form = LoginForm(request.form)
	if request.method == 'POST' and form.validate_on_submit():
		user = User.login_user(form.username.data, request.remote_addr,
				form.password.data)
		# Check if we got a user or not
		if user is None:
			# Authentication failed
			flash(_("<strong>Your username and/or password was \
					incorrect.</strong> Please try again."))
			return redirect(url_for('auth.login'))

		# Set the username and organization on the session
		session['login'] = user.username
		session['organization'] = user.organizations[0]._id

		# Assume authentication was successful
		if 'login_destination' in session:
			flash(_("<strong>You've logged in successfully!</strong>"), 'success')
			return redirect(session.pop('login_destination'))
		else:
			return redirect(url_for('dashboard.index'))
	else:
		# Check if the user is logged in...if so, redirect to
		# the dashbard as they shouldn't have to login again.
		if 'login' not in session:
			return render_template('auth/login.html', form=form)
		else:
			return redirect(url_for('dashboard.index'))


@module.route('/logout')
def logout():
	if 'login' in session:
		session.clear()

	flash(_("<strong>You've been logged out successfully.</strong>"), 'success')
	return redirect(url_for('auth.login'))
