MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
MONGODB_USERNAME = 'semaphore-testing'
MONGODB_PASSWORD = 'testing123'
MONGODB_DB = 'semaphore'

AUTH_COLNAME = 'authentication'
ORG_COLNAME = 'organizations'
TICKET_COLNAME = 'tickets'
SETTINGS_COLNAME = 'settings'
PRODUCT_COLNAME = 'products'
