from flask import current_app, url_for, g
from semaphore.core.util.curry import curry
from semaphore.core.tickets.models import PRIORITIES, STATUSES

def datetimeformat(value):
	if 'TIMESTAMP_FORMAT' in g.user.settings:
		return value.strftime(g.user.settings['TIMESTAMP_FORMAT'])
	return value.strftime('%m/%d/%Y %H:%M')

def readable_priority(value):
	return PRIORITIES[int(value)] if int(value) < len(PRIORITIES) else value

def readable_status(value):
	return STATUSES[int(value)] if int(value) < len(STATUSES) else value

def display_name(value):
	if 'DISPLAY_NAME' in g.user.settings:
		return g.user.settings['DISPLAY_NAME'] % value
	return '%(first)s %(last)s' % value

FILTERS = {'timestamp_format': datetimeformat, 'readable_priority':
		readable_priority, 'readable_status': readable_status, 'display_name':
		display_name}
