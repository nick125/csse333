#!/usr/bin/env python
#
# Manually creates a organization
#
from search_queries import *
from semaphore.core.organizations.models import Organization

config_connect()


def build_organization():
	u = Organization()
	u.name = raw_input("Organization name: ")
	u.active = True
	u.save()
	return u


def testing():
	for x in range(0, 10):
		org = Organization()
		org.name = 'Test Organization ' + str(x)
		org.active = True
		org.save()
		print org

if __name__ == "__main__":
	testing_mode = raw_input("Testing? : (1 - Yes , 0 - No)")
	if testing_mode is '1':
		testing()
		print "Test organizations inserted."
	else:
		organization = build_organization()
		print organization
