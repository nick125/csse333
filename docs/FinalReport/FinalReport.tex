\documentclass[10pt,letterpaper]{article}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{fancyhdr}
%\usepackage{minted} 
%\usepackage{tabularx}
%\usepackage{fontspec}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{color}
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\usepackage[xindy,toc]{glossaries}
\usepackage[left=0.75in, top=0.75in, right=0.75in, bottom=0.75in]{geometry}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\pagestyle{fancy}
\setlength{\parindent}{0in}

\begin{document}
\lhead{\textbf{Semaphore} - Final Report}
% Title page
\begin{titlepage}
\begin{center}
	\textsc{\large Introduction to Database Systems} \\
	\textsc{\large Department of Computer Science} \\
	\textsc{\large Rose-Hulman Institute of Technology} \\
	\textsc{\large Terre Haute, Indiana} \\[0.5cm]

	\HRule \\[0.5cm]
	\textsc{\Huge Semaphore} \\[0.5cm]
	\textsc{\Large Final Report} \\[0.5cm]
	\HRule \\[1.5cm]
	
	\begin{minipage}{0.4\textwidth}
	\begin{flushleft} \large
	\emph{Authors:}\\
	\textsc{Nicholas Kamper} \\ kampernj@rose-hulman.edu\\[0.5cm]
	\textsc{Andy Chen} \\ chena1@rose-hulman.edu\\
	\end{flushleft}
	\end{minipage}
	\begin{minipage}{0.4\textwidth}
	\begin{flushright} \large
	\HRule \\[1.0cm]
	\HRule
	\end{flushright}
	\end{minipage}

	\vfill
	{\large \today}
\end{center}
\end{titlepage}
% Table of Contents
\newpage
\renewcommand\contentsname{Table of Contents}
\tableofcontents
\newpage
% Executive Summary
\section{Executive Summary} 
\label{sec:execsum}
In this document, we intend to describe the problem that led to the development of Semaphore and the solution implemented to address this problem. We also want to cover our design and some of the rational behind various design choices. Finally, we want to summarize some of the challenges we faced and how we worked around them.

% Introduction
\section{Introduction}
\label{sec:intro}
In modern software development paradigms, software defect trackers play a critical role in allowing development teams to efficiently produce quality software products. With Semaphore, we intend on developing a high-quality cloud-based software defect tracker that allows organizations to efficiently manage their organization's software defects. \\
In this document, we will analyze our solution and our approach, in regards to its effectiveness, implemented by our team to address the problem outlined in previous documents.

\section{Problem Description}
\label{sec:hlps}
We developed a high-performance software defect tracker for software development firms of all sizes -- ranging from a few people working out of their basement to a large multi-national development team spanning in the thousands. We wanted to offer quality software defect tracking to organizations at a reasonable price, without requiring a significant IT or licensing expenditures that several of our competitors require. Our primary goal was to design and develop a system for software development firms to efficiently track their software defects as they move through the software defect lifecycle.

\subsection{Scope}
The primary scope of our project was limited to tracking defects and feature requests in a software product. As a result, we needed to track Organizations, Users (such as developers and testers), Products, and Defects. \\

The required features of the product for it to be considered useful are the following, as per our final problem description.

\subsubsection{Organizations and Users}
Being available as a cloud-based solution, we must be able to handle several organizations using a single installation of the software. As such, we must be able to track all information relative to an Organization. We also need to support multiple users being able to login to the system using individual logins.\\ 

\subsubsection{Products}
As an organization may develop several complicated products, we must be able to track different products. Each issue will target a specific ``product".
\subsubsection{Issues}
Being a software defect/issue tracker, we must be able to track software defects and issues. Each defect, known also as a "ticket," will be stored in the organization's database. Each ticket must contain a brief title, a detailed description, a status and assigned developer, an affected product, and identification of who reported the bug and those that are affected by the bug.\\

In addition, each ticket must also allow for ``comments" to be made on the ticket, where a comment can be placed due to attributes (such as the status) being modified or by a user placing a comment. Each comment should be capable of having a file attached, such as a patch or log files. 

Initially, we intend on being able to do a full-text search of the titles of issues in the database. 
\subsubsection{Basic Access Control Levels (ACLs)}
In a large development organization, not everyone should have privileges to do certain sensitive operations, such as delete tickets, add/modify/delete users, etc. As such, we must implement a basic form of access controls, allowing certain users to be designated as being able to perform sensitive operations. 

\section{Solution Description}
\subsection{Flask: Web Framework}
We chose to use Flask, a Python-based microframework, for web development. The framework is rooted from the Werkzeug WSGI framework and Jinja 2, a modern templating language for Python, which features template inheritance, easy debugging, and an optional sandboxed template execution environment. \\

Flask comes with a built-in development server/debugger, supports secure client side sessions, and is extensively documented. We chose this framework in consideration of its performance (compared to the likes of PHP) and ease of coding due to being written in Python. We felt that working in a language such as Java or C\# would have put us at a disadvantage, due to web frameworks for Java or C\# being rather verbose. 

\subsubsection{WTForms: Web Form Toolkit}
WTForms is a framework that allowed us to define custom forms and have the form field code automatically generated for us with cross-site request forgery protection built in. In addition, it has allowed us to keep a separation of the form code from the rest of the code in our views. The toolkit also has several validation components built in while allowing us to define custom validators if we wished. 

\subsubsection{Twitter Bootstrap: Templating Framework}
To ease the development load while still keeping Semaphore aesthetically pleasing, we decided to use the Twitter bootstrap toolkit in developing the interface of our application. The toolkit features the use of a 12-column grid and responsive cross-platform compatibility, in which components are scaled according to a range of resolutions and devices to provide a consistent experience. The toolkit utilizes HTML5 and CSS3 to provide these aesthetically pleasing and intuitive user experience. 

\subsection{MongoDB: Database Management System (DBMS)}
For our database management system, we chose to use MongoDB. MongoDB is a scalable, high performance, and open source NoSQL database. It features document-oriented storage with dynamic schemas, which offer simplicity and unparalleled flexibility compared to a traditional RDBMS. MongoDB also offers index support on any attribute and scalability through sharding and replica sets. Unlike other NoSQL databases that we considered, MongoDB was unique in that it still offered a way to dynamically query the database via its document-based query language.

\subsection{MongoEngine: Python-MongoDB Models Framework}
MongoEngine is an object-document mapper used to connect Python and MongoDB. We opted to use MongoEngine instead of interacting with MongoDB using a lower-level library, such as pymongo, as a result of the object mapping and validation facilities provided by MongoEngine. While we were interested in using MongoDB for its schemaless properties, we still needed some level of schema for our objects.

\section{Design and Architecture}
\subsection{High-level Architecture}
We initially designed Semaphore to be flexible. As a result, our design became quite complicated. 

\subsection{Authentication and Authorization}
We built the authentication framework in Semaphore for future expandability. In Semaphore, we have the concept of an ``authentication token," a generic object that describes how a particular user should be able to login. Different implementations of these ``tokens" would allow for different authentication methods to be supported. We only implemented support for locally-stored hashed passwords, but it would be trivial to implement support for user authentication over LDAP or Active Directory.\\

We initially designed our permissions model to be flexible, allowing us to quickly add new permissions to Semaphore. However, during the end of our project, we discovered a bit of a design flaw with our current implementation of permissions in Semaphore -- we only kept track of permissions on a user and on an organization, which meant that we couldn't grant permissions to a specific user for a specific organization. In the future, we would likely redesign our permissions model to fix this flaw, possibly by having a settings store for a user/organization pair.\\

In the current product, each user and organization has a Settings object. A settings object is a simple key-value store with string keys and integer values. Each bit of the value represented a different permission, in the style of UNIX file permissions. So, for example, a settings store might have a TICKET permission with value equal to $CREATE | MODIFY | DELETE$, where $CREATE$, $MODIFY$, and $DELETE$ are some value in the form of $(1<<n)$.

\subsection{Changesets}
One of the most important parts of a software defect tracker is to track the progression of an issue as it traverses the software development process. As such, a significant portion of our efforts was put into retaining and managing history.\\

To store history, we developed the concept of a ``changeset" for a ticket. Every time a ticket is modified, a new changeset is created with the previous and new states of the ticket. In theory, we should be able to go through all of the changesets on a ticket and be able to ``replay" the history of a ticket.\\

To implement changesets, we utilized MongoEngine's signals support to call a trigger prior to any saves. Inside of this hook, we traverse all of the fields on the ticket to check if they have changed from their initial state. For collections, such as lists, we will retrieve the previous state of the collection, convert both the previous and current states into sets and perform difference operators to get the list of items added and removed from the collection. This changeset is then attached to the changesets collection on the ticket and the save goes through.

\subsection{Indices}
We used a limited number of indices in Semaphore to improve our retrieval performance. We decided to limit the number of
indices due to performance limitations, as a large number of indices can result in poor insertion/update performance. All of 
objects have a clustered index on their unique ID, which is a 384-bit UUID assigned by MongoDB. In addition, our Ticket object
has a descending index on the last\_modified timestamp, as well as indices on tags, title, and changesets.involves (a list of people that a particular changeset might be ``interesting" to). Finally, our users are indexed on username.

\subsection{Stored Procedures}
Initially, we planned on using stored procedures wherever possible. However, we found out that MongoDB's stored Javascript procedures were unsuitable for our use due to the fact that a MongoDB stored procedure will write-lock the entire database during their execution. As a result, we had to revert to our logic being in our database models. This was part of the motivation behind the reason we initially adopted MongoKit (and eventually, MongoEngine). 

\subsection{Critique of our Design and Implementation}
We tried to design our application to be flexible, which I believe gives us a good base to work with. However, there are 
a few flaws that we would eventually like to address.
\subsubsection{Flawed Permissions Model}
As mentioned above, our permissions model has a significant flaw -- it only keeps track of permissions on an organization
level and a user level, but not on a user/organization pair. In the future, we would like to resolve this by having a 
permissions/settings store on a user/organization pair. 
\subsubsection{Messy Views and Inconsistency}
Some of the code and design for our views was rather poorly developed. For example, our switch to using forms more extensively
(e.g., having the forms populate our database models) happened late in the game, meaning that some of our forms populate the
database model and some of them still have code in the view to populate objects. Eventually, we would like to clean this code
up 
\subsubsection{Questionable Scalability}
One of the things that we would've liked to put more work into would be performance and scalability. Some areas in our code,
such as the search, might be really expensive operations that could use optimization.
\subsubsection{Conclusion}
Overall, we believe that we've built a decent design -- our changesets model allows us to keep track of the history of a ticket
in a way that we believe is superior to any other way to implement history. If we were to clean up some of the parts of the 
design, we believe that the code might be usable for production.

\section{Key Challenges}

\subsection{Technical Challenges}
As a team, we faced several technical challenges when we were developing Semaphore. \\
\\
Neither of us in the team had experience with many of the frameworks or toolkits we had intended on using. This created some difficulty in developing the application and had increased development time because of the variable learning curve required in all the frameworks/toolkits we were using. Fortunately, Nick had been able to become proficient in the frameworks extremely quickly and helped Andy learn enough to sufficiently lend a hand in the project. While documentation was plentiful, nothing could replace experience with implementing a wide variety of APIs/frameworks/toolkits. For someone who wasn't as experienced in web development using Python, it was definitely something that took time to get used to and become proficient. 
\\
\\At the start of the project, the initial database connectivity framework we had chosen, MongoKit, had bugs that made the framework unusable -- it couldn't handle transforming our Ticket objects from MongoDB into Python objects. As a result, development on Semaphore had to be halted and rolled back to a state where we would be able to use MongoEngine as our object-document mapper of choice. To do this, we had to rewrite our object models, testing code and some other signifiicant parts of our code to use MongoEngine. The transition to MongoEngine was fairly smooth, and, in the end, gave us a working connectivity layer in the application to work with.

\subsection{Management Challenges}
There were several management challenges faced when we were developing Semaphore. \\
\\
Because our team had only consisted of two people, allocation of work was not the easiest thing to do, especially when considering that the disparity, in regards to skill level, between the two of us was significant. Naturally, it is easy to divide work to do when the skill levels of members in a team are about relatively the same, but it doesn't work so well when the disparity between the members is significant, especially when it came to measuring the number of manhours it might've taken one member to do something simple alongside another member implementing much more complicated functionality. \\
\\
In addition, new features would easily creep up during the course of application development. Many little additions, such as the possible AJAX editing implementation (as mentioned in one of our previous status reports), had seemed easy to add and would add a lot of value to the application, in terms of user-friendliness and aesthetics. However, these features would've also taken a significant number of manhours to implement. To address this, we had set reasonable goals in our intermediate status reports and during development time to minimize what would be the existence of essentially vaporware. This approach worked quite well because the goals were perceived as reachable, which gave us some peace of mind that we would not be overworking ourselves for most of the project. It was really important to know where to draw the line and not get over the top ambitious, even though the spirit was still there. \\
\\
It wasn't uncommon that we had a habit of overestimating the amount of time we had to implement certain features, or underestimate the complexity and amount of time it would take to implement certain features in the application. Given that we were operating with frameworks and toolkits that we had not operated with before, it was certainly a mistake to make time estimations based off the development speed during a situation, in which we would be extremely familiar with the framework or toolkit we were using. As a result, we had re-evaluated the situation and made the required feature set align more realistically to what could be done within the short time-frame we had to develop Semaphore. Nonetheless, we were impressed with the amount of progress we had made after our initial technical difficulties during development. 

\section{Entity-Relational Diagram}
\includegraphics[height=9in]{CSSE333ERF.png}

\section{Relational / Object Schema}

\subsection{Relational Schema}
\textsl{Note: Email address will be used as the unique identifier of an user. In addition, changesets now take care of comments. } \\
\label{sec:relschema}

Product(\underline{product\_id} , product\_name, \underline{org\_id}, active) \\
Organization ( \underline{org\_id}, org\_name, org\_address, org\_email , active) \\ 
Ticket( \underline{ticket\_id} , ticket\_status, ticket\_description, product\_id, active )  \\ 
Changeset (\underline{ticket\_id}, \underline{timestamp}, types, involves, fields) \\
User( user\_password, \underline{user\_email} ,  user\_phone, org\_id, active ) \\
LoginHistory( \underline{user\_email}, successful, ipaddr, timestamp) \\
Setting ( \underline{setting\_id}, setting\_object ) \\ 
User\_configured ( \underline{settings\_id}, \underline{user\_id}, active ) \\ 
Org\_configured ( \underline{settings\_id} , \underline{org\_id} ) \\



\subsection{Object Schema}
\label{sec:objschema}
\emph{ Please note: MongoDB objects inherently have an \_id field } 
\lstset{ %
  language= XML ,                % the language of the code
  basicstyle=\footnotesize,           % the size of the fonts that are used for the code
  numbers=left,                   % where to put the line-numbers
  numberstyle=\footnotesize,          % the size of the fonts that are used for the line-numbers
  stepnumber=2,                   % the step between two line-numbers. If it's 1, each line 
                                  % will be numbered
  numbersep=5pt,                  % how far the line-numbers are from the code
  backgroundcolor=\color{white},      % choose the background color. You must add \usepackage{color}
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  frame=none,                   % adds a frame around the code
  tabsize=2,                      % sets default tabsize to 2 spaces
  captionpos=b,                   % sets the caption-position to bottom
  breaklines=true,                % sets automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  title=\lstname,                   % show the filename of files included with \lstinputlisting;
                                  % also try caption instead of title
  numberstyle=\tiny\color{gray},        % line number style
  keywordstyle=\color{blue},          % keyword style
  commentstyle=\color{dkgreen},       % comment style
  stringstyle=\color{mauve},         % string literal style
  escapeinside={\%*}{*)},            % if you want to add a comment within your code
  morekeywords={*,...}               % if you want to add more keywords to the set
}
{\footnotesize	
\begin{multicols}{2}
\begin{lstlisting}

User { 
	username (email) :  str, 
	organization: [ 
		ids...] 
	password: { 
	'algd': str, 
	'salt' : str, 
	'hash' : str 
	} 
	name : { 
	'first' : str, 
	'last' : str 
	}
	loginHistory: [
		{  'IP' : str.
		'timestamp' : str,
		'organization_id': str
		}
	]
	Settings: Settings [object]	
	Permissions: Permissions
}

Product {
	product : id,
	product_name : str,
	org_id : int

}

Organization {
	name : str, 
	Settings : Settings[object]
	Permissions : Permissions [object]
}


Ticket {
	owner: id,
	title : str,
	body : str,
	priority: int ,
	tags: { ...str...}
	comments: [
		{ owner : id,
		body: str,
		}
	changesets : [
		{ owner : id,
		timestamp : str,
		type : str,
		fields : [
		{ key , old , new }
		}
	]

Settings {
	settings = [
	{Key, Value}
	]
}

Permissions {
	permissions = [
	{ action , boolean }	 (i.e.  User: Allow/Deny , Group : str, Organizations)
	]
}
 

\end{lstlisting}
\end{multicols}
}
\end{document}
