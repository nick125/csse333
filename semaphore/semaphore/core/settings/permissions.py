from flask import current_app as app, g, request

READ = 1
WRITE = 2
CREATE = 4
MODIFY = 8
DELETE = 16
MANAGE = 32

TICKET = "TICKET"
COMMENT = "COMMENT"
USERS = "USERS"
ORGANIZATION = "ORGANIZATION"
PRODUCTS = "PRODUCTS"

"""
Permission resolution order:
	1) Check for resource on user, if the user's permission store contains the
	value, then check for permission level.
	2) Check for the resource on the organization's permission store, and check
	against that permission level.
	3) Accept the request.
"""


def resolve_permission(resource, level):
	"""
	Determines whether the current user/organization has the requested
	permission level for the given resource.
	"""
	# Check for the permission on the user
	if resource in g.user.permissions:
		return (g.user.permissions[resource] & level) == level
	elif resource in g.organization.permissions:
		return (g.organization.permissions[resource] & level) == level
	else:
		return True
