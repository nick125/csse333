from semaphore import create_app

if __name__ == "__main__":
    app = create_app()
    app.run(app.config['BIND_HOST'], app.config['BIND_PORT'])
