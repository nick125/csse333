from flask import g
from babel.support import LazyProxy


def ugettext(s):
	if getattr(g, 'translation', None):
		return g.translation.ugettext(s)
	else: 
		# TODO: Fix this.
		return s

def ugettext_lazy(s):
	return LazyProxy(ugettext, s)

