from flask import Blueprint, flash, g, redirect
from flask import request, session, url_for, current_app as app
from functools import wraps
from semaphore.core.i18n import ugettext as _

__all__ = ["module", "before_request_hooks"]

module = Blueprint('auth', __name__, template_folder="../../templates")

def populate_user():
	""" Populates the user object on each request """
	if 'login' in session:
		g.user = models.User.get_user(session['login'])


before_request_hooks = [populate_user]

from . import models
from . import views
