#
# From http://code.activestate.com/recipes/52549-curry-associating-parameters-with-a-function/#

class curry: 
	def __init__(self, fun, *args, **kwargs):
		self.fun = fun
		self.args = args[:]
		self.kwargs = kwargs.copy()

	def __call__(self, *args, **kwargs):
		if kwargs and self.kwargs:
			kw = self.kwargs.copy()
			kw.update(kwargs)
		else:
			kw = kwargs or self.kwargs

		return self.fun(*(self.args + args), **kw)
