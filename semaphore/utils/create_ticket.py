#!/usr/bin/env python
#
# Manually creates a ticket
#
from config import *
from pymongo.objectid import ObjectId
from mongoengine import connect
from semaphore.core.auth import models
from semaphore.core.organizations import models as orgModels
from semaphore.core.tickets import models as ticketModels
from semaphore.core.organizations import models as orgModels
from utils.search_queries import *

config_connect()


def build_ticket():
	ticket = Ticket()
	ticket_title = raw_input("Title of Ticket: ")
	ticket_body = raw_input("Body of ticket : ")

	ticket_priority = None
	while True:
		ticket_priority = int(raw_input("Priority of Ticket (0 to 10): "))
		if ticket_priority < 11 and ticket_priority > -1:
			break
		else:
			print 'Invalid Ticket Priority Entered'
			
	ticket_status = None
	while ticket_status is None:
		ticket_status = int(raw_input("Ticket Status from 0-6"))
		if ticket_status < 7 and ticket_status > -1:
			break
		else:
			print 'Invalid status entered.'

	comments = {}
	attachments = {}
	changesets = {}
	settings = {}

#	ticket = Ticket(organization = t[1], active = true, owner=t[0], product=t[2]
#		title= ticket_title, body= ticket_body, priority=ticket_priority, status= ticket_status)
	ticket.save()


def testing():
	product = prompt_product()
	user = prompt_user()
	org = prompt_organization()
	for x in range(0, 10):
		u = ticketModels.Ticket()
		u.product = product
		u.owner = user
		u.organization = org
		u.title = u'Test Ticket with Priority' + str(x)
		u.body = u'I am a test ticket with priority' + str(x)
		u.status = 0  # TODO: Evaluate creating more complicated tickets!
		u.priority = x
		u.active = True
		u.attachments = []
		u.comments = []
		# Done!
		u.save(owner=user)
		print u


if __name__ == "__main__":
	testing_mode = raw_input("Testing? : (1 - Yes , 0 - No) ")
	if testing_mode is '1':
		testing()
		print "Test tickets inserted."
	else:
		ticket = build_ticket()
		print ticket
