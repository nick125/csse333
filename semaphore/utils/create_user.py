#!/usr/bin/env python
# 
# Manually creates a organization
#
from search_queries import *
from semaphore.core.auth.models import User, NameToken, Password
from semaphore.core.organizations.models import Organization
from semaphore.core.settings import permissions
from semaphore.core.settings.models import Settings, Permissions

config_connect()

DEFAULT_SETTINGS = {
		'TIMESTAMP_FORMAT': '%m/%d/%Y %H:%M',
}

DEFAULT_PERMISSIONS = {
		permissions.TICKET: permissions.READ|permissions.WRITE|permissions.CREATE|permissions.MODIFY|permissions.DELETE,
}


def build_user():
	u = User()
	u.username = raw_input("Username (email): ")
	u.name = NameToken()
	u.name.first, u.name.last = raw_input("Name: ").split(" ")
	# Get the organizations
	for org in multiprompt_organization():
		u.organizations.append(org)

	# Setup the password
	p = Password()
	p.set_password(raw_input("Password: "))
	p.save()
	u.password = p
	u.active = True

	# Create a settings store 
	for item in DEFAULT_SETTINGS.items():
		u.settings[item[0]] = item[1]

	# Create a permissions store
	for item in DEFAULT_PERMISSIONS.items():
		u.permissions[item[0]] = item[1]

	u.save()
	return u

if __name__ == "__main__":
	user = build_user()
	print user
