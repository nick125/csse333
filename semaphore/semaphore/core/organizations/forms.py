from pymongo.objectid import ObjectId
from flaskext.wtf import Form, HiddenField, Required, SubmitField, TextField, \
		TextAreaField, SelectField

class CreateOrganizationForm(Form):
	name = TextField('Title', validators=[Required()])

class EditOrganizationForm(Form):
	name = TextField('Title', validators=[Required()])
	active = SelectField('active', choices =[('true', 'Active'),('false', 'Inactive')], coerce=boolean, validators=[Required()])