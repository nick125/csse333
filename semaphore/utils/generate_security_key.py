##
## Creates a security key
##

from string import ascii_letters, digits, punctuation
from random import choice

DEFAULT_LENGTH = 64
CHARSET = [x for x in ascii_letters + digits + punctuation]

print "".join([choice(CHARSET) for x in xrange(DEFAULT_LENGTH)])
