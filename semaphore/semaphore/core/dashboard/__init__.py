from flask import Blueprint

__all__ = ["module"]

module = Blueprint('dashboard', __name__, template_folder="../../templates")

from .views import *
