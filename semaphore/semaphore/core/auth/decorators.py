from flask import redirect, flash, current_app as app, redirect,\
		session, request, url_for
from functools import wraps

from semaphore.core.i18n import ugettext as _
from config import AUTH_IGNORE_FLASH

__all__ = ["login_required"]

def login_required(f):
	""" States that a resource requires authentication """
	@wraps(f)
	def decorated_function(*args, **kwargs):
		if 'login' not in session:
			app.logger.debug(
					'User tried to access authenticated path. Redirecting...')
			session['login_destination'] = request.path

			# Only show flashes for paths not ignored
			if request.path not in AUTH_IGNORE_FLASH:
				flash(_('<strong>You must be logged in to view that\
						page.</strong> Please login and we\'ll redirect you.'),
						'error')

			return redirect(url_for('auth.login'))
		return f(*args, **kwargs)
	return decorated_function

