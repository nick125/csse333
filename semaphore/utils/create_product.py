#!/usr/bin/env python
# 
# Manually creates a product
#

from config import *
from pymongo.objectid import ObjectId
from mongoengine import connect
from utils.search_queries import *
from semaphore.core.tickets.models import *
from semaphore.core.organizations.models import *

config_connect()


def build_product():
	u = Product()
	u.name = raw_input("Product Name:")
	u.organization = prompt_organization()
	u.active = True
	u.save()
	return u


def testing():
	# Get the organization
	org = prompt_organization()

	for x in range(0, 20):
		u = Product()
		u.organization = org
		u.name = 'Test Product' + str(x)
		u.active = True
		u.save()
		print u

if __name__ == "__main__":
	testing_mode = raw_input("Testing? : (1 - Yes , 0 - No) ")
	if testing_mode is '1':
		testing()
		print "Test products inserted."
	else:
		product = build_product()
		print product
