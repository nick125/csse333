from string import ascii_letters, digits, punctuation

DEFAULT_HASH = 'SHA-2-256'
ENABLE_SALT = True
NEW_SALT_ON_CHANGE = True 
SALT_LENGTH = 64
SALT_CHARSET = [x for x in ascii_letters + digits + punctuation] 
