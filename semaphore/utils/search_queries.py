#Search Queries
from pymongo.objectid import ObjectId
from config import MONGODB_DB, MONGODB_USERNAME, MONGODB_PASSWORD
from mongoengine import connect
from semaphore.core.organizations.models import Organization
from semaphore.core.auth.models import User
from semaphore.core.tickets.models import Ticket, Product, Changeset


def config_connect():
	connect(MONGODB_DB, username=MONGODB_USERNAME, password=MONGODB_PASSWORD)


def find_user(user):
	user = User.objects(username=user)
	if user is None:
		return None
	else:
		return user


def find_organization(org):
	org = Organization.objects(id=ObjectID(org))
	if org is None:
		return None
	else:
		return org


def find_product(product_name, product_organization):
	product = Product.objects(name=product_name,
			organization=product_organization)
	if product is None:
		return None
	else:
		return product


##
# The prompts
##
def prompt_organization(prompt="Enter an organization ID: "):
	while True:
		org_id = raw_input(prompt)
		if org_id is not "":
			org = Organization.get_organization(org_id)
			if org is not None:
				return org


def multiprompt_organization(prompt="Enter org IDs, comma separated: "):
	org_ids = raw_input(prompt).split(",")
	orgs = []
	for org_id in org_ids:
		org = Organization.get_organization(org_id)
		if org is not None:
			orgs.append(org)
	return orgs


def prompt_user(prompt="Enter an owner username: "):
	while True:
		username = raw_input(prompt)
		if username is not "":
			user = User.get_user(username)
			if user is not None:
				return user


def prompt_product(prompt="Enter a product ID: "):
	while True:
		product_id = raw_input(prompt)
		if product_id is not "":
			product = Product.get_product(product_id)
			if product is not None:
				return product
