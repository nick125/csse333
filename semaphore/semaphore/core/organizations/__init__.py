from flask import Blueprint, flash, g, redirect
from flask import request, session, url_for, current_app as app

from . import models

module = Blueprint('organization', __name__, template_folder="../../templates")

def populate_organization():
	if 'login' in session and 'organization' in session:
		app.logger.debug('Getting organization \'%s\' for \'%s\'' %
				(session['organization'], session['login']))
		g.organization = models.Organization.get_organization(
				session['organization'])

	else:
		app.logger.debug("User doens't have have an organization")

before_request_hooks = [populate_organization]
