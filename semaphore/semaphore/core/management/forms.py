from pymongo.objectid import ObjectId
from flaskext.wtf import Form, HiddenField, Required, SubmitField, TextField, \
		TextAreaField, SelectField, Length, Email, PasswordField

__all__ = ["ProductForm", "OrganizationForm","UserForm", "UserForm2"]

def coerceBool(x):
	if type(x) == bool:
		return str(x)
	elif type(x) == str or type(x) == unicode:
		return x.lower() == 'true'
	else:
		return x

class UserForm(Form):
	uid = HiddenField('uid')
	name = TextField('User Email', validators=[Email(message='Invalid email address.')])
	password = PasswordField('Password', validators=[Length(min=1,max=64, message='Invalid Password')])
	active = SelectField('Active', choices =[(True, 'Active'),(False, 'Inactive')], 
			coerce=bool, default=True)
	submit = SubmitField('Submit')
	
class UserForm2(Form):
	uid = HiddenField('uid')
	name = TextField('Full Name', validators=[Length(min=1,max=64,message='Invalid Full Name')])
	username = TextField('User Email', validators=[Email(message='Invalid email address.')])
	password = PasswordField('Password', validators=[Length(min=1,max=64, message='Invalid Password')])
	active = SelectField('Active', choices =[(True, 'Active'),(False, 'Inactive')], 
			coerce=bool, default=True)
	submit = SubmitField('Submit')

class OrganizationForm(Form):
	name = TextField('Organization Name', 
			validators=[Length(min=1,max=64, message='Invalid Name')])
	active = SelectField('Active', choices =[(True, 'Active'),(False, 'Inactive')], 
			coerce=bool, default=True)
	submit = SubmitField('Make Changes')


class ProductForm(Form):
	pid = HiddenField('pid')
	name = TextField('Title', validators=[Length(min=1,max=64, message='Invalid Name')])
	submit = SubmitField('Submit')
	active = SelectField('Active', choices =[(True, 'Active'),(False, 'Inactive')], 
			coerce=coerceBool, default=True)
