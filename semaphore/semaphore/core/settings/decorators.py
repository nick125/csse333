from flask import redirect, url_for, flash
from functools import wraps
from .permissions import resolve_permission

__all__ = ["permission_required"]

def permission_required(resource, level):
	""" States that a resource requires certain permissions """
	def decorator(f):
		@wraps(f)
		def decorated_function(*args, **kwargs):
			# Check for permissions
			if resolve_permission(resource, level):
				return f(*args, **kwargs)
			flash(_('You do not have permission to view that resource.'))
			return redirect(url_for('dashboard.index'))
		return decorated_function
	return decorator
