#!/usr/bin/env python
#
# Does a lot of handling of authentication stuff
#

from flask import current_app as app
from hashlib import sha256
from random import choice
from string import ascii_letters, digits, punctuation

DEFAULT_LENGTH = 64
CHARSET = [x for x in ascii_letters + digits + punctuation]
DEFAULT_ALGO = "SHA-2-256"

# Expects classes that have a hexdigest() method, as hashlib hashing methods
# do.
ALGO_MAP = {
        "SHA-2-256": sha256
        }


def fetch_orgs(org_list):
	"""
	Returns a list of organizations
	"""
	app.logger.debug(org_list)
	return [get_orgCol().find_one({'_id': org}) for org in org_list]

def hash_algo(user, password):
    """
    Finds an authentication hashing algorithm based on the 'algo' field
    """

    value = password + user['password']['salt']
    return ALGO_MAP[user['password']['algo']](value).hexdigest()


def check_password(user, password):
    """
    Checks if the password provided is correct.
    """
    if user is None:
        return None

    return hash_algo(user, password) == user['password']['hash']


def create_hash():
    """
    Creating a new random salt string
    """
    return "".join([choice(CHARSET) for x in xrange(DEFAULT_LENGTH)])


def get_authCol():
    """
    Returns the auth collection
    """
    return app.mongo.db[app.config['AUTH_COLNAME']]

def get_orgCol():
	"""
	Returns the organizations collection
	"""
	return app.mongo.db[app.config['ORG_COLNAME']]

def get_user(username):
    """
    Returns a user for a given username. Returns None if there are no
    active users with the given username.
    """
    user = get_authCol()['users'].find_one({'username': username,
        'active': True})
    if user is None:
        app.logger.debug('No such user "%s" in auth database %s'
                % (username, app.config['AUTH_COLNAME']))
    return user


def authenticate_user(username, password):
    """
    Tries to authenticate a user. Returns None if the user wasn't
    authenticated, otherwise returns the user's document
    """
    # Find the user's document
    user = get_user(username)
    return user if check_password(user, password) else None


def change_password(user, oldPassword, newPassword):
    """
    Processes a request to change the user's password. Returns False if the
    password change was not successful.
    """
    if user is None:
        app.logger.debug("Tried to change password for a None user")
        return False

    # Check the authentication token provided
    if check_password(user, password):
        # Set the new password
        user['password']['salt'] = create_hash()
        user['password']['algo'] = DEFAULT_ALGO
        user['password']['hash'] = hash_algo(user, newPassword)

        get_authCol()['users'].update({'username': user.username, 'active':
            True}, user)
