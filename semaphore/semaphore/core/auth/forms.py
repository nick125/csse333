from flaskext.wtf import Form, Email, Required, SubmitField, TextField,\
		PasswordField, BooleanField
from flask import g

class LoginForm(Form):
	username = TextField('Username', validators=[Required(), Email(message='Username is invalid.')])
	password = PasswordField('Password', validators=[Required()])
	submit = SubmitField('Login')

class CreateUserForm(Form):
	username = TextField('Username', validators=[Required(), Email(message='Username is invalid.')])
	name = TextField('Name', validators=[Required()])
	password = PasswordField('Password', validators=[Required()])
	submit = SubmitField('Create')
	
class EditUserForm(Form):
	password = PasswordField('Password')
	f_name = TextField('First Name', validators=[Required()])
	l_name = TextField('Last Name', validators=[Required()])
	active = BooleanField('active', validators=[Required()])
	submit = SubmitField('Make Changes')

	def populate_obj(self, user):
		super(EditUserForm,self).populate_obj(user)
		user.f_name = user.name.first
		user.l_name = user.name.last
		
			
	def bind_runtime_fields(self, user):
		f_name.default = user.name.first
		l_name.default = user.name.last

class EditUserProfileForm(Form):
	password = PasswordField('Password')
	f_name = TextField('First Name', validators=[Required()])
	l_name = TextField('Last Name', validators=[Required()])
	submit = SubmitField('Make Changes')
	
	def populate_obj(self, user):
		super(EditUserProfileForm,self).populate_obj(user)
		user.f_name = user.name.first
		user.l_name = user.name.last
	
	def bind_runtime_fields(self, user):
		f_name.default = user.name.first
		l_name.default = user.name.last
